class SecondaryQuestionNode:
	"""
	Holds the information for the second set of questions
	-question: string rep of the question
	-genre: string rep of the genre the question gives a point to
	-next: holds ref to next question node
	"""
	def __init__(self, question, genre):
		self.question = question
		self.genre = genre
		self.next = None
