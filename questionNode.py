class QuestionNode:
	"""
	Holds the info for the first mandatory questions
	-question: string rep of question
	-yes_response: dic of points assigned to genre if yes
	-no_response: dic of points assigned to genre if no
	-next: ref to next node question
	"""
	def __init__(self, question, yes_response, no_response):
		self.question = question
		self.yes_response = yes_response
		self.no_response = no_response
		self.next = None
