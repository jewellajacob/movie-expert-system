import requests
import random

class MovieSuggestions:
	def __init__(self, genre):
		"""
		genre is a string rep of the chosen genre
		movie_list will be returned as the three titles
		"""
		self.genre = genre
		self.movie_list = []

	def find_the_movies(self):
		"""
		controls the flow of the movie recommendation system
		"""
		self.genre_to_int()
		self.call_api()
		return self.movie_list

	def genre_to_int(self):
		"""
		the url for the api requires that the genre is passed as an int
		"""
		if self.genre == 'drama':
			self.genre = 18
		elif self.genre == 'comedy':
			self.genre = 35
		elif self.genre == 'action':
			self.genre = 28
		elif self.genre == 'horror':
			self.genre = 27
		elif self.genre == 'documentary':
			self.genre = 99

	def call_api(self):
		"""
		This calls the movie db api to find three movies with the set
		genre.
		"""
		#sets a random number between 1 and 10 to help in selecting three random movie selections
		for x in range(10):
		  random_num = random.randint(1,11)
		#the api is called and a dic of movies is returned
		URL = 'https://api.themoviedb.org/3/discover/movie?api_key=6fa89bbccaed760d1779c573f0b47f4a&with_genres{}'.format(self.genre)
		request = requests.get(url = URL)
		data = request.json()
		#the data is truncated to three movies at a random index
		data = data['results'][random_num:random_num+3]
		for movie in data:
			self.movie_list.append(movie['title'])
