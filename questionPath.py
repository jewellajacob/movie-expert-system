import json
import operator

from questionNode import QuestionNode
from secondaryQuestionNode import SecondaryQuestionNode

class QuestionNavigation:

	def __init__(self):
		self.head_question = None
		self.genre_count = {
			"drama" : 0,
			"action" : 0,
			"horror" : 0,
			"comedy" : 0,
			"documentary" : 0
		}
		self.top_genre = None
		self.second_genre = None
		self.third_genre = None
		self.the_genre = None

	def navigate_questions(self):
		self.set_initial_questions()
		self.add_initial_points()
		self.set_additional_questions(self.find_top_genres())
		self.add_additional_points()
		self.find_the_genre()
		return self.the_genre

	def set_initial_questions(self):
		question_build = QuestionBuild()
		self.head_question = question_build.initial_build()

	def set_additional_questions(self, genre_list):
		question_build = QuestionBuild()
		self.head_question = question_build.additional_build(genre_list)

	def add_initial_points(self):
		current_question = self.head_question
		while current_question != None:
			response = int(input(current_question.question))
			if response:
				for genre in current_question.yes_response:
					self.genre_count[genre] += current_question.yes_response[genre]
			else:
				for genre in current_question.no_response:
					self.genre_count[genre] += current_question.no_response[genre]

			if current_question.next != None:
				current_question = current_question.next
			else:
				current_question = None

	def add_additional_points(self):
		current_question = self.head_question
		while current_question != None:
			response = int(input(current_question.question))
			if response:
				self.genre_count[current_question.genre] += 1

			if current_question.next != None:
				current_question = current_question.next
			else:
				current_question = None

	def find_top_genres(self):
		genre_count_list = []
		genre_top_list = []
		for genre in self.genre_count:
			genre_count_list.append(self.genre_count[genre])
		top_index, self.top_genre = max(enumerate(genre_count_list), key=operator.itemgetter(1))
		genre_count_list[top_index]=-1
		genre_top_list.append(top_index)
		second_index, self.second_genre = max(enumerate(genre_count_list), key=operator.itemgetter(1))
		genre_count_list[second_index]=-1
		genre_top_list.append(second_index)
		third_index, self.third_genre = max(enumerate(genre_count_list), key=operator.itemgetter(1))
		genre_count_list[third_index]=-1
		genre_top_list.append(third_index)
		return(genre_top_list)

	def find_the_genre(self):
		genre_count_list = []
		the_genre = None
		for genre in self.genre_count:
			genre_count_list.append(self.genre_count[genre])
		top_index, self.top_genre = max(enumerate(genre_count_list), key=operator.itemgetter(1))
		if top_index == 0:
			the_genre = 'drama'
		elif top_index == 1:
			the_genre = 'action'
		elif top_index == 2:
			the_genre = 'horror'
		elif top_index == 3:
			the_genre = 'comedy'
		elif top_index == 4:
			the_genre = 'documentary'
		self.the_genre = the_genre

class QuestionBuild:
	"""
	contains the functions for building the question path
	"""

	def __init__(self):
		self.head_question = None
		self.previous_question = None

	"""
	Uses questions.json to determine the first round of questions. currently,
	there will be four questions. Each question has a dic for the amount of points
	to be assigned to each genree
	"""
	def initial_build(self):
		with open('data/questions.json', 'r') as json_data:
			question_info = json.load(json_data)
		for question_number in question_info:
			question_node = QuestionNode(question_info[question_number]['question'], question_info[question_number]['yes'], question_info[question_number]['no'])
			if self.head_question == None:
				self.head_question = question_node
				self.previous_question = self.head_question
			else:
				self.previous_question.next = question_node
				self.previous_question = self.previous_question.next

		return self.head_question

	def additional_build(self, genre_array):
		self.head_question = None
		data_file = None
		current_genre = None
		for genre in genre_array:
			if genre == 0:
				current_genre = 'drama'
				data_file = 'data/drama.json'
			elif genre == 1:
				current_genre = 'action'
				data_file = 'data/action.json'
			elif genre == 2:
				current_genre = 'horror'
				data_file = 'data/horror.json'
			elif genre == 3:
				current_genre = 'comedy'
				data_file = 'data/comedy.json'
			elif genre == 4:
				current_genre = 'documentary'
				data_file = 'data/documentary.json'
			with open(data_file, 'r') as json_data:
				question_info = json.load(json_data)
			for question_number in question_info:
				question_node = SecondaryQuestionNode(question_info[question_number]['question'], current_genre)
				if self.head_question == None:
					self.head_question = question_node
					self.previous_question = self.head_question
				else:
					self.previous_question.next = question_node
					self.previous_question = self.previous_question.next
		return self.head_question
