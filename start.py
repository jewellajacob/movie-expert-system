import requests

from questionPath import QuestionNavigation
from movieApi import MovieSuggestions

def main():
	"""
	This is the function that the program runs from. It calls the question nav
	system and grabs a genre. It grabs some movie suggestions with a db api.
	And prints the UI
	"""
	question_nav = QuestionNavigation()
	the_genre = question_nav.navigate_questions()
	print("We are suggesting that you pick a {0} genre film.".format(the_genre))
	movie_api = MovieSuggestions(the_genre)
	movie_suggestions = movie_api.find_the_movies()

	print("Some movie suggestions: ")
	for suggestion in movie_suggestions:
		print(" - {0}".format(suggestion))


main()
